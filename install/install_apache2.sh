#!/bin/bash

echo "Installing apache2"

sudo apt update
sudo apt upgrade

sudo apt install apache2

sudo systemctl enable apache2

sudo systemctl start apache2

echo "Installing apache2 successful"

echo "Installing PHP"

sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update

sudo apt install php8.3 php8.3-cli php8.3-fpm php8.3-mysql php8.3-curl php8.3-gd php8.3-mbstring php8.3-xml php8.3-zip php8.3-fileinfo php8.3-iconv

#sudo a2dismod php7.x       # Replace 7.x with the version currently enabled
#sudo a2enmod php8.3

sudo systemctl restart apache2



