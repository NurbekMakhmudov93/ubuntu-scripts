#!/bin/bash

echo "Installing NFile for xampp"

cd /opt/lampp/htdocs || exit

git clone https://gitlab.com/NurbekMakhmudov93/nfile.git

chown -R www-data:www-data /opt/lampp

chmod -R 755 /opt/lampp

chmod -R a+rwx /opt/lampp/htdocs
chmod -R a+rwx /opt/lampp/etc/extra

chmod -R a+rwx /home

echo "Installing NFile for xampp successful"




