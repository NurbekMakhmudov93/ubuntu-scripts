#!/bin/bash

echo "Installing nginx"

sudo apt update
sudo apt upgrade

#apt install nginx -y
apt install nginx

systemctl start nginx

echo "Installing PHP"

apt-get install ca-certificates apt-transport-https software-properties-common
sudo apt-get install ca-certificates apt-transport-https software-properties-common
sudo add-apt-repository ppa:ondrej/php

sudo apt-get update

sudo apt install php8.3 php8.3-cli php8.3-fpm php8.3-mysql php8.3-curl php8.3-gd php8.3-mbstring php8.3-xml php8.3-zip php8.3-sqlite3 php8.3-fileinfo php8.3-iconv

sudo mkdir /var/www/welcome
sudo chown -R $USER:$USER /var/www/welcome

echo "
server {
    listen 80;
    server_name welcome www.welcome;
    root /var/www/welcome;

    index index.html index.htm index.php;

    location / {
        try_files $uri $uri/ = 404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php8.3-fpm.sock;
     }

    location ~ /\.ht {
        deny all;
    }
}
" > /etc/nginx/sites-available/welcome


sudo ln -s /etc/nginx/sites-available/welcome /etc/nginx/sites-enabled/
sudo unlink /etc/nginx/sites-enabled/default
sudo nginx -t
sudo systemctl reload nginx

echo "<?php phpinfo(); ?>" > /var/www/html/index.php




