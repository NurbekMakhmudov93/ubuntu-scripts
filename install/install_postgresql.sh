#!/bin/bash

echo "Install PostgreSQL"

sudo apt update

sudo apt install postgresql postgresql-contrib

sudo systemctl enable postgresql

sudo systemctl start postgresql

echo "Installing PostgreSQL successful"
echo "Secure PostgreSQL"

sudo -i -u postgres

createuser --interactive

#Enter name of role to add: root
#Shall the new role be a superuser? (y/n) y

createdb my_db
psql

ALTER USER root WITH PASSWORD 'root';
\q

exit

echo "Secure PostgreSQL successful"


echo "Install pgAdmin"

# Install the public key for the repository (if not done previously):
curl -fsS https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo gpg --dearmor -o /usr/share/keyrings/packages-pgadmin-org.gpg

# Create the repository configuration file:
sudo sh -c 'echo "deb [signed-by=/usr/share/keyrings/packages-pgadmin-org.gpg] https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'

# Install for both desktop and web modes:
#sudo apt install pgadmin4

# Install for desktop mode only:
#sudo apt install pgadmin4-desktop

# Install for web mode only:
sudo apt install pgadmin4-web

# Configure the webserver, if you installed pgadmin4-web:
sudo /usr/pgadmin4/bin/setup-web.sh

#admin@gmail.com
#pawwsord

#http://127.0.0.1/pgadmin4





