#!/bin/bash

echo "Installing XAMPP 8.2.12"

wget https://sourceforge.net/projects/xampp/files/XAMPP%20Linux/8.2.12/xampp-linux-x64-8.2.12-0-installer.run

chmod +x xampp-linux-x64-8.2.12-0-installer.run

sudo ./xampp-linux-x64-8.2.12-0-installer.run

sudo /opt/lampp/lampp start

#sudo /opt/lampp/lampp security

#sudo /opt/lampp/lampp stop

#Run the Uninstaller
#cd /opt/lampp
#sudo ./uninstall

#Remove XAMPP Directory
#sudo rm -rf /opt/lampp

