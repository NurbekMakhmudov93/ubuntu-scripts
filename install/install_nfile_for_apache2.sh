#!/bin/bash

echo "Installing NFile for apache2"

cd /var/www/html || exit

git clone https://gitlab.com/NurbekMakhmudov93/nfile.git

chown -R www-data:www-data /var/www/html

chmod -R 755 /var/www/html

chmod -R a+rwx /var/www/html

chmod -R a+rwx /home

echo "Installing NFile for apache2 successful"


