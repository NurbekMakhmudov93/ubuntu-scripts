#!/bin/bash
echo "==================================================="
echo "Date"
date
echo "==================================================="
echo "Uptime"
uptime
echo "==================================================="
echo "Memory Usage"
free -m
echo "==================================================="
echo "Network Usage"
ip a
echo "==================================================="

